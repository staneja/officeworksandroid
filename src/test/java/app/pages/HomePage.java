package app.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import auto.common.AppiumWrapper;

public class HomePage extends AppiumWrapper {

    public static By userName = By.id("userName");
    public static By loginButton = By.id("login");
    public static By allowButton = By.id("com.android.packageinstaller:id/permission_allow_button");
    public static By toolTipMsgButton = By.xpath("//android.widget.TextView[@text='Browse by category']");

    public static By findIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab'][0]");
  
    public static By storesIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab'][1]");
    public static By listIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab'][2]");
    public static By loginIcon = By.xpath("//*[@class='android.support.v7.app.ActionBar$Tab'][3]");
    
    public void tapFindIcon(){
    	tapElement(findIcon);
    }
    
    
   public void tapstoresIcon(){
    	tapElement(storesIcon);
    }
    
    
    public void taplistIcon(){
    	tapElement(listIcon);
    }
    
    
    public void taploginIcon(){
    	tapElement(loginIcon);
    }
    
    
    
    
    public void clickLogin() {

        tapElement(loginButton);

    }
    
  
    
    public void clicktoolTipMsgButton() {

        tapElement(toolTipMsgButton);

    }
    
    public void clickAllowButton() {

        tapElement(allowButton);

    }

}
