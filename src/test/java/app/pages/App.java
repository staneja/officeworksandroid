package app.pages;

import auto.common.InitAppium;
import auto.utils.AutoUtilities;
import auto.utils.FluentAssert;

public class App extends InitAppium {

    public HomePage homePage = null;
    public FluentAssert fluentAssert = null;
    public AutoUtilities utils = null;
    public LoginPage loginpage=null;
    public FindPage findPage=null;
    public ProductPage productPage= null;
    public CartPage cartPage= null;

    public ForgotPasswordPage forgotPasswordPage = null;
    public RegistrationPage registrationPage = null;

    public ProductDetailsPage productdetailspage= null;


    public App() {

        utils = new AutoUtilities();
        // Call this method with the test data json file name if needed for the project
        utils.loadTestData(System.getProperty("user.dir") + "/TestData/sampledata.json");

        // Initializing the fleuntAssert object for global use.
        fluentAssert = new FluentAssert();

        // Initializing the various pages of the app for use across all tests
        homePage = new HomePage();
        loginpage = new LoginPage();
        forgotPasswordPage = new ForgotPasswordPage();
        findPage = new FindPage();
        productPage= new ProductPage();
        cartPage = new CartPage();

        registrationPage = new RegistrationPage();

        productdetailspage = new ProductDetailsPage();

    }

}
