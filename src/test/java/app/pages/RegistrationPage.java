package app.pages;

import org.openqa.selenium.By;

import auto.common.AppiumWrapper;

public class RegistrationPage extends AppiumWrapper{
	public static By navigateUpButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
	public static By registrationLabel = By.xpath("//android.widget.TextView[@text='Registration']");
	public static By taxInvoicesStaticMessage = By.xpath("//android.widget.TextView[@text='Associate this account with your business and receive tax invoices in your company name.']");
	
	public static By logInInfoLabel = By.xpath("//android.widget.TextView[@text='Log In Information']");
	public static By emailTextFieldLabel = By.xpath("//TextInputLayout[@text='Email *']");
	public static By emailTextFieldValue = By.xpath("//TextInputLayout[@text='Email *']//android.widget.EditText");
	public static By passwordTextFieldLabel = By.xpath("//TextInputLayout[@text='Password *']");
	public static By passwordTextFieldValue = By.xpath("//TextInputLayout[@text='Password *']//android.widget.EditText");
	public static By confirmPasswordTextFieldLabel = By.xpath("//TextInputLayout[@text='Confirm Password *']");
	public static By confirmPasswordTextFieldValue = By.xpath("//TextInputLayout[@text='Confirm Password *']//android.widget.EditText");
	public static By requiredFieldsStaticMessage = By.xpath("//android.widget.TextView[@text='* indicates required fields']");
	
	public static By accountForBuisnessLabel = By.xpath("//android.widget.TextView[@text='Is this account for business?']");
    public static By accountForBusinessCheckbox = By.xpath("//android.widget.CheckBox");
	public static By companyInformationLabel = By.xpath("//android.widget.TextView[@text='Company Information']");
	public static By abnTextFieldLabel = By.xpath("//TextInputLayout[@text='ABN *']");
	public static By abnTextFieldValue = By.xpath("//TextInputLayout[@text='ABN *']//android.widget.EditText");
	public static By companyNameTextFieldLabel = By.xpath("//TextInputLayout[@text='Company Name *']");
	public static By companyNameTextFieldValue = By.xpath("//TextInputLayout[@text='Company Name *']//android.widget.EditText");
	
    public static By contactInformationLabel = By.xpath("//android.widget.TextView[@text='Contact Information']");
    public static By firstNameTextFieldLabel = By.xpath("//TextInputLayout[@text='First Name *']");
    public static By firstNameTextFieldValue = By.xpath("//TextInputLayout[@text='First Name *']//android.widget.EditText");
    public static By lastNameTextFieldLabel =  By.xpath("//TextInputLayout[@text='Last Name *']");
    public static By lastNameTextFieldValue = By.xpath("//TextInputLayout[@text='Last Name *']//android.widget.EditText");
    public static By primaryPhoneNumberTextFieldLabel = By.xpath("//TextInputLayout[@text='Primary Phone Number *']");
    public static By primaryPhoneNumberTextFieldValue = By.xpath("//TextInputLayout[@text='Primary Phone Number *']//android.widget.EditText");
    public static By otherPhoneNumberTextFieldLabel = By.xpath("//TextInputLayout[@text='Other Phone Number']");
    public static By otherPhoneNumberTextFieldValue = By.xpath("//TextInputLayout[@text='Other Phone Number']//android.widget.EditText");
    
    public static By addressLabel = By.xpath("//android.widget.TextView[@text='Address']");
    public static By buildingTextFieldLabel = By.xpath("//TextInputLayout[@text='Building']");
    public static By buildingTextFieldValue = By.xpath("//TextInputLayout[@text='Building']//android.widget.EditText");
    public static By addressTextFieldLabel = By.xpath("//TextInputLayout[@text='Address *']");
    public static By addressTextFieldValue = By.xpath("//TextInputLayout[@text='Address *']//android.widget.EditText");
    public static By suburbTextFieldLabel = By.xpath("//TextInputLayout[@text='City/Suburb *']");
    public static By suburbTextFieldValue = By.xpath("//TextInputLayout[@text='City/Suburb *']//android.widget.EditText");
    public static By postcodeTextFieldLabel = By.xpath("//TextInputLayout[@text='Postcode *']");
    public static By postcodeTextFieldValue = By.xpath("//TextInputLayout[@text='Postcode *']//android.widget.EditText");
    public static By stateSpinner = By.id("au.com.officeworks.mobile:id/state_spinner");
    public static By stateValue = By.id("android:id/text1");
    
    public static By disclaimerStaticMessage = By.id("au.com.officeworks.mobile:id/text_disclaimer");
    public static By disclaimerCheckbox = By.xpath("//android.widget.CheckBox");
    public static By registerButton = By.xpath("//android.widget.Button[@text='Register']");
    
    public static By passwordDoNotMatchErrorMessage = By.xpath("//android.widget.TextView[@text='Does not match password']");
    
    // Form validation error messages shown
	public static By invalidEmailErrorMessage = By.xpath("//android.widget.TextView[@text='LogonId must be a valid email address at most 40 characters']");
	public static By invalidPhNumErrorMessage = By.xpath("//android.widget.TextView[@text='Invalid phone number format']");
	public static By invalidPostcodeErrorMessage = By.xpath("//android.widget.TextView[@text='Invalid Postcode']");
	public static By emailIdAlreadyExistsErrorMessage = By.xpath("//android.widget.TextView[@text='An account with this email address already exists.']");
	public static By invalidPasswordErrorMessage = By.xpath("//android.widget.TextView[@text='Password requires at least six characters including at least one letter and one digit']");
	public static By invalidSuburbStatePostcodeErrorMessage = By.xpath("//android.widget.TextView[@text='Invalid combination of suburb, state and postcode']");

	// Form validation success messages shown
	public static By thankyouSuccessMessage = By.xpath("//android.widget.TextView[@text='Thank you for registering']");
	public static By accountRegisteredSuccessMessage = By.xpath("//android.widget.TextView[@text='Your account is now active and ready to use.']");
	
	public static By alertOkButton = By.xpath("//android.widget.Button[@text='OK']");
}
