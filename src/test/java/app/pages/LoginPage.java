package app.pages;

import org.openqa.selenium.By;

import auto.common.AppiumWrapper;

public class LoginPage extends AppiumWrapper {
	
	public static By registerButton = By.xpath("//android.widget.Button[@text='Register']");
	public static By loginTabStaticTextMessage = By.xpath("//android.widget.Button[@'New customers can create an account with us to make their shopping experience easier. Existing customers can simply log in.']");

	public static By loginText = By.xpath("//android.widget.TextView[@text='Log In']");
	public static By navigateUpButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
	public static By loginTextFieldLabel = By.xpath("//TextInputLayout[@text='Email or Log In ID']");
	// public static By loginTextFieldValue = By.xpath("//android.widget.EditText[@index='Email or Log In ID']");
	public static By passwordTextFieldValue = By.id("au.com.officeworks.mobile:id/password");
	public static By loginStaticTextMessage = By.xpath("//android.widget.TextView[@text='To log in, use your email address and password. For older accounts, use your Log In ID instead of your email address.']");
	public static By loginButton = By.xpath("//android.widget.Button[@text='Log in']");
	public static By ForgotPasswordButton =  By.xpath("//android.widget.Button[@text='Forgot password']");
	
	 public void tapLoginButton(){
	    	tapElement(loginButton);
	    }
	 
	 public void tapForgotPasswordButton(){
	    	tapElement(ForgotPasswordButton);
	    }
	 
	 public void loginToApp(){
		 
	 }

	 public void tapRegisterButton(){
		 tapElement(registerButton);
	 }
}
