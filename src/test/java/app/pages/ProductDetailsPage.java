package app.pages;

import org.openqa.selenium.By;

import auto.common.AppiumWrapper;

public class ProductDetailsPage extends AppiumWrapper {

	public static By titleButtonBackArrow = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
	public static By titleStaticHeader = By.xpath("//android.widget.TextView[@text='Product Details']");
	// next line contains body
	public static By bodyStaticHeaderMessage = By.xpath("//android.widget.TextView[@text='Papermate Sharpie Expo Writing Essentials Pack 33 Piece']");
	public static By bodyStaticProductCode = By.xpath("//android.widget.TextView[@text='Product code: PES1910264']");
	public static By bodyStaticImage = By.xpath("//android.support.v4.view.ViewPager//android.widget.ImageView[@index=0]");
	public static By bodySrollCircles = By.id("au.com.officeworks.mobile:id/circles");
	public static By bodyStaticPrice = By.xpath("//android.widget.TextView[@text='$19.49']");
	public static By bodyStaticQuantity = By.xpath("//android.widget.TextView[@text='Quantity:']");
	public static By bodyStaticQuantityNumber = By.xpath("//android.widget.TextView[@text='1']");
	
	public static By bodyButtonAddToCard = By.xpath("//android.widget.Button[@text='Add to cart']");
	
	public static By bodyButtonAddToMyList = By.xpath("//android.widget.Button[@text='Add to my list']");
	
	public static By bodyStaticDeliverToDoor = By.xpath("//android.widget.TextView[@text='Deliver to Door']");
	public static By bodyStaticDispatched = By.xpath("//android.widget.TextView[@text='Dispatched NEXT business day']");
	
	public static By bodyButtonClickAndCollect = By.xpath("//android.widget.TextView[@text='Click & Collect']");
	public static By bodyStaticClickAndCollect = By.xpath("//android.widget.TextView[@text='Collect in  1-2 business days']");
	
	public static By bodyButtonShopInStore = By.xpath("//android.widget.TextView[@text='Shop In Store']");
	public static By bodyStaticShopInStore = By.xpath("//android.widget.TextView[@text='Available in store NOW']");

	
	public static By bodyStaticYourStore = By.xpath("//android.widget.TextView[@text='Your store']");
	public static By bodyButtonLocation = By.xpath("//android.widget.TextView[@text='Bourke St, Melbourne Officeworks']");
	
	public static By bodyStaticReviews = By.xpath("//android.widget.TextView[@text='Reviews']");
	public static By bodyStaticReviewCount = By.id("au.com.officeworks.mobile:id/reviews_count");

	

	
	
}
                                                    