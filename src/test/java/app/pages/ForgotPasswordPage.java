package app.pages;

import org.openqa.selenium.By;

import auto.common.AppiumWrapper;

public class ForgotPasswordPage extends AppiumWrapper {
	
	public static By navigateUpButton = By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']");
	public static By forgotPwdLabel = By.xpath("//android.widget.TextView[@text='Forgot Password']");
	public static By forgotPwdStaticTextMessage = By.xpath("//android.widget.TextView[@text='Enter your email or Log In ID below and we\'ll send you an email with instructions on how to re-new your password.']");
	public static By emailOrLoginIdFieldLabel = By.xpath("//TextInputLayout[@text='Email or Log In ID']");
	public static By sendEmailButton = By.xpath("//android.widget.Button[@text='Send Email']");
	public static By emailIdTextField = By.xpath("//android.widget.EditText");
	//or public static By emailIdTextField = By.xpath("//android.widget.EditText[@index=0]");
	public static By resetPasswordSuccessMessage = By.id("android:id/message");
	public static By resetPasswordOkButton = By.id("android:id/button1");
	public static By resetPasswordFailureMessage = By.id("au.com.officeworks.mobile:id/snackbar_text");
	
	public void tapNavigateUpButton(){
		tapElement(navigateUpButton);
	}
	
	public void enterEmailId(String email){
		typeValue(email, emailIdTextField);
	}
	
	public void tapsendEmailButton(){
		tapElement(sendEmailButton);
	}
	
	public void tapResetPasswordOkButton(){
		tapElement(resetPasswordOkButton);
	}
	
	public String getresetPasswordFailureMessageText(){
		return readValue(resetPasswordFailureMessage);
	}
}
