package app.pages;

import org.openqa.selenium.By;

import auto.common.AppiumWrapper;


public class DeliveryPage extends AppiumWrapper 
{
	
	public static By dialougCloseButton = By.xpath("//android.widget.Button[@content-desc='Close']");
    public static By checkOutButton = By.xpath("//android.widget.TextView[@text='Checkout']");
	public static By deliveryToDoorButton = By.xpath("//android.view.View[@resource-id='deliver-to-door-option']");
	public static By clickAndCollectButton = By.xpath("//android.view.View[@resource-id='click-and-collect-option']");
	public static By showItemButton = By.xpath("//android.view.View[@content-desc='SHOW ITEMS']");
	public static By hideItemButton = By.xpath("//android.view.View[@content-desc='HIDE ITEMS']");
	
	
	
	
	
	
	
	public void tapDialougCloseButton()
    {tapElement(dialougCloseButton);}
	
	public void tapCheckOutButton()
    {tapElement(checkOutButton);}
	
	public void tapDeliveryToDoorButton()
    {tapElement(deliveryToDoorButton);}
	
	public void tapClickAndCollectButton()
    {tapElement(clickAndCollectButton);}
}
