package app.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import app.pages.App;
import app.pages.ForgotPasswordPage;
import app.pages.LoginPage;
import auto.common.InitAppium;

public class ForgotPasswordPageTest extends TestCase {

	@Test
	public void checkForgotPasswordPageElements() {
		homePage.taploginIcon();
		loginpage.tapLoginButton();
		loginpage.tapForgotPasswordButton();
		AssertJUnit.assertTrue(forgotPasswordPage.checkElementExists(forgotPasswordPage.navigateUpButton));
		AssertJUnit.assertTrue(forgotPasswordPage.checkElementExists(forgotPasswordPage.forgotPwdLabel));
		// AssertJUnit.assertTrue(forgotPasswordPage.checkElementExists(forgotPasswordPage.forgotPwdStaticTextMessage));
		AssertJUnit.assertTrue(forgotPasswordPage.checkElementExists(forgotPasswordPage.emailOrLoginIdFieldLabel));
		AssertJUnit.assertTrue(forgotPasswordPage.checkElementExists(forgotPasswordPage.sendEmailButton));

	}

	@Test
	public void navigatingBackToLoginScreen() {
		homePage.taploginIcon();
		loginpage.tapLoginButton();
		loginpage.tapForgotPasswordButton();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(forgotPasswordPage.navigateUpButton));
		// InitAppium.driver.manage().timeouts().implicitlyWait(200,
		// TimeUnit.SECONDS);

		forgotPasswordPage.tapNavigateUpButton();
		AssertJUnit.assertTrue(loginpage.checkElementExists(loginpage.loginText));
	}

	@Test
	public void resettingPassword() {
		homePage.taploginIcon();
		loginpage.tapLoginButton();
		loginpage.tapForgotPasswordButton();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(forgotPasswordPage.emailIdTextField));
		// InitAppium.driver.manage().timeouts().implicitlyWait(200,
		// TimeUnit.SECONDS);

		// Entering invalid email id and resetting password fails
		forgotPasswordPage.enterEmailId("invalidEmail");
		forgotPasswordPage.tapsendEmailButton();
		AssertJUnit.assertEquals("Something went wrong. Please try again.",
				forgotPasswordPage.getresetPasswordFailureMessageText());

		// Entering valid email id and resetting password successfully
		forgotPasswordPage.enterEmailId("dev.test@outware.com.au");
		forgotPasswordPage.tapsendEmailButton();
		forgotPasswordPage.checkElementExists(forgotPasswordPage.resetPasswordSuccessMessage);
		forgotPasswordPage.checkElementExists(forgotPasswordPage.resetPasswordOkButton);
		forgotPasswordPage.tapResetPasswordOkButton();
		AssertJUnit.assertTrue(loginpage.checkElementExists(loginpage.loginText));
	}

}
