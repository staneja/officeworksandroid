package app.tests;

import javax.media.jai.RegistryElementDescriptor;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import app.pages.App;
import app.pages.RegistrationPage;
import io.appium.java_client.SwipeElementDirection;

public class RegistrationPageTest extends TestCase {

	@Test
	public void checkRegistrationPageLoginInfoElements() {
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.navigateUpButton));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.registrationLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.taxInvoicesStaticMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.logInInfoLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.emailTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.emailTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.passwordTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.passwordTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.confirmPasswordTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.confirmPasswordTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.requiredFieldsStaticMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBuisnessLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBusinessCheckbox));
	}

	
	@Test
	public void checkRegistrationPageContactInfoElements() {
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));

		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.contactInformationLabel));

		// INFO SwipeElementDirection.DOWN
		// INFO registrationPage.swipeScreen("down"); -> This is taking directly to the bottom of the screen
		// INFO driver.swipe(50, driver.findElement(RegistrationPage.contactInformationLabel).getLocation().getY() + 300, 50,
		//		driver.findElement(RegistrationPage.contactInformationLabel).getLocation().getY(), 1000);
		// TODO implement this -> assertField(RegistrationPage.firstNameTextFieldLabel);
        int starty = (int) (driver.manage().window().getSize().getHeight() * 0.75);
        int endy = (int) (driver.manage().window().getSize().getHeight() * 0.20);
        driver.swipe(50, starty, 50, endy, 1000);
		
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.firstNameTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.firstNameTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.lastNameTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.lastNameTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.primaryPhoneNumberTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.primaryPhoneNumberTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.otherPhoneNumberTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.otherPhoneNumberTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.requiredFieldsStaticMessage));	
	}
	
	public void assertField(By field){
		if(registrationPage.checkElementExists(field)){
			AssertJUnit.assertTrue(registrationPage.checkElementExists(field));
			return;
		}else{
			registrationPage.swipeScreen("UP");
			int xstart = (driver.manage().window().getSize().getHeight())/2;
			int xend = 50;	 
		}
	}


	@Test
	public void checkRegistrationPageAddressElements(){
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));
		
        int starty = (int) (driver.manage().window().getSize().getHeight() * 0.75);
        int endy = (int) (driver.manage().window().getSize().getHeight() * 0.20);
        driver.swipe(50, starty, 50, endy, 1000);
        driver.swipe(50, starty, 50, endy, 1000);
        
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.addressLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.buildingTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.buildingTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.addressTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.addressTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.suburbTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.suburbTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.postcodeTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.postcodeTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.stateSpinner));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.stateValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.requiredFieldsStaticMessage));
	    
		registrationPage.swipeScreen("down");
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.disclaimerStaticMessage));
		AssertJUnit.assertEquals("* I have read and agreed to the Officeworks Privacy policy and Collection Statement and the Officeworks Terms of Use.", registrationPage.readValue(RegistrationPage.disclaimerStaticMessage));
	    AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.disclaimerCheckbox));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.registerButton));
		
	}
}