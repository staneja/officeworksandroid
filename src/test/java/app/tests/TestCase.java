package app.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


import app.pages.App;

public class TestCase extends App {

	
	@BeforeSuite
	public void before() throws Exception{
		
		System.out.println("Before Class called");
		try{
		if(homePage.checkElementExists(homePage.allowButton)){
			 homePage.clickAllowButton();
		  		homePage.clicktoolTipMsgButton();
		  		driver.closeApp();
		}	}
		catch(Exception e) {
			
		}
	}
	
    @BeforeMethod	
    public void beforeEachTest() throws Exception {
    	System.out.print("Start App called");
    	
    	
    	try {
			driver.launchApp();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @AfterMethod
    public void afterEachTest() throws Exception{
    	System.out.print("Close App called");
    	try {
			driver.closeApp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @AfterSuite
    public void afterSuite() throws Exception {
    	driver.removeApp("au.com.officeworks.mobile");
    	driver.quit();
    }
}
