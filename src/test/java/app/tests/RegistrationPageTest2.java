package app.tests;

import javax.media.jai.RegistryElementDescriptor;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import app.pages.App;
import app.pages.LoginPage;
import app.pages.RegistrationPage;
import io.appium.java_client.SwipeElementDirection;

public class RegistrationPageTest2 extends TestCase {

	@Test
	public void checkRegistrationPageLoginInfoElements() {
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.navigateUpButton));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.registrationLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.taxInvoicesStaticMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.logInInfoLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.emailTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.emailTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.passwordTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.passwordTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.confirmPasswordTextFieldLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.confirmPasswordTextFieldValue));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.requiredFieldsStaticMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBuisnessLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBusinessCheckbox));
	}

	@Test
	public void checkRegistrationPageContactInfoElements() {
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));

		/* INFO testing all cases in assertFieldTrue method
		assertFieldTrue(RegistrationPage.contactInformationLabel);
		assertFieldTrue(RegistrationPage.lastNameTextFieldValue);
		assertFieldTrue(RegistrationPage.taxInvoicesStaticMessage);
		assertFieldTrue(RegistrationPage.registerButton);
		assertFieldTrue(By.xpath("//abc")); */
		
		assertFieldTrue(RegistrationPage.contactInformationLabel);
		assertFieldTrue(RegistrationPage.firstNameTextFieldLabel);
		assertFieldTrue(RegistrationPage.firstNameTextFieldValue);
		assertFieldTrue(RegistrationPage.lastNameTextFieldLabel);
		assertFieldTrue(RegistrationPage.lastNameTextFieldValue);
		assertFieldTrue(RegistrationPage.primaryPhoneNumberTextFieldLabel);
		assertFieldTrue(RegistrationPage.primaryPhoneNumberTextFieldValue);
		assertFieldTrue(RegistrationPage.otherPhoneNumberTextFieldLabel);
		assertFieldTrue(RegistrationPage.otherPhoneNumberTextFieldValue);
		assertFieldTrue(RegistrationPage.requiredFieldsStaticMessage);	
	}
	
	/*
	If the field is not found on the visible part of the screen, then this method scrolls through the whole screen and tries to find the element
	 1. first finds the element on the current visible screen
	 2. If not found then scrolls to the next part of the screen and tries to find the element (This check is done as mostly the current element
	    will be shown next to the previous element and this might be on the start of next page of the screen when we scroll)
	 3. If not found in the next part, then user scrolls to the very top of the screen and tries to find the element
	 4. If not found, then as the control is on the starting of the screen, we scroll part by part on the screen till we reach the bottom 
	    (we will be searching for the element on each part when we are scrolling)
	*/
	public void assertFieldTrue(By locator){
		int screenHeight = driver.manage().window().getSize().getHeight();
		int starty = (int)(screenHeight * 0.5);
           
		if((registrationPage.checkElementExists(locator) )){
			AssertJUnit.assertTrue(registrationPage.checkElementExists(locator));
			return;
		} else {
			// scrolling to the next part
			driver.swipe(50, starty, 50, 50, 1000);
			if(registrationPage.checkElementExists(locator)){
				AssertJUnit.assertTrue(registrationPage.checkElementExists(locator));
				return;
			}
			// registrationPage.swipeScreen("up");
			// Scrolling to the top of the screen. This is done twice to reach the very top of the screen. 
			// Cause if we give the command once, its scrolling up but its not reaching the very top of the screen
			driver.swipe(50, 700, 50, screenHeight-10, 1000);
			driver.swipe(50, 700, 50, screenHeight-10, 1000);
			if(registrationPage.checkElementExists(locator)){
				AssertJUnit.assertTrue(registrationPage.checkElementExists(locator));
				return;
			} else {
				for (int i=0; i<=10; i++){
					// scrolling to the next part every time
					driver.swipe(50, starty, 50, 50, 1000);
					if(registrationPage.checkElementExists(locator)){
						AssertJUnit.assertTrue(registrationPage.checkElementExists(locator));
						return;
					}
				}
			}
			// Fails as the element is not found on the whole screen
			AssertJUnit.assertTrue(registrationPage.checkElementExists(locator));
			return;
		}
	}
	
	/*
	Here we are checking that the element we are searching for is not present on the screen
	 1. scrolls to the very top of the screen and tries to find the element (In case if the element is found the test fails)
	 2. If not found, then as the control is on the starting of the screen, we scroll part by part on the screen till we reach the bottom 
	    (we will be searching for the element on each part when we are scrolling)
	*/
	public void assertFieldFalse(By locator){
		int screenHeight = driver.manage().window().getSize().getHeight();
		int starty = (int)(screenHeight * 0.5);
		
		//registrationPage.swipeScreen("up");
		driver.swipe(50, 700, 50, screenHeight-10, 1000);
		driver.swipe(50, 700, 50, screenHeight-10, 1000);
		if(registrationPage.checkElementExists(locator)){
			AssertJUnit.assertFalse(registrationPage.checkElementExists(locator));
			return;
		} else {
			for (int i=0; i<=10; i++){
				driver.swipe(50, starty, 50, 50, 1000);
				if(registrationPage.checkElementExists(locator)){
					AssertJUnit.assertFalse(registrationPage.checkElementExists(locator));
					return;
				}
			}
		}
		// Passes as the element is not found on the whole screen
		AssertJUnit.assertFalse(registrationPage.checkElementExists(locator));
		return;
	}

	@Test
	public void checkRegistrationPageAddressElements(){
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));
        
		assertFieldTrue(RegistrationPage.addressLabel);
		assertFieldTrue(RegistrationPage.buildingTextFieldLabel);
		assertFieldTrue(RegistrationPage.buildingTextFieldValue);
		assertFieldTrue(RegistrationPage.addressTextFieldLabel);
		assertFieldTrue(RegistrationPage.addressTextFieldValue);
		assertFieldTrue(RegistrationPage.suburbTextFieldLabel);
		assertFieldTrue(RegistrationPage.suburbTextFieldValue);
		assertFieldTrue(RegistrationPage.postcodeTextFieldLabel);
		assertFieldTrue(RegistrationPage.postcodeTextFieldValue);
		assertFieldTrue(RegistrationPage.stateSpinner);
		assertFieldTrue(RegistrationPage.stateValue);
		assertFieldTrue(RegistrationPage.requiredFieldsStaticMessage);
		assertFieldTrue(RegistrationPage.disclaimerStaticMessage);
		AssertJUnit.assertEquals("* I have read and agreed to the Officeworks Privacy policy and Collection Statement and the Officeworks Terms of Use.", registrationPage.readValue(RegistrationPage.disclaimerStaticMessage));
		assertFieldTrue(RegistrationPage.disclaimerCheckbox);
		assertFieldTrue(RegistrationPage.registerButton);
	}
	
	@Test
	public void checkBusinessAccountFileds(){
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));
		
		// This check is made so that the user is scrolled to the 'accountForBuisnessLabel' field. And there we can select/unselect the checkbox
		// (as there is a chance that the checkbox might not be present on the visible area of the screen, so we need to scroll to the area where checkbox is)
		assertFieldTrue(RegistrationPage.accountForBuisnessLabel);
		registrationPage.tapElement(RegistrationPage.accountForBusinessCheckbox);
		assertFieldTrue(RegistrationPage.companyInformationLabel);
		assertFieldTrue(RegistrationPage.abnTextFieldLabel);
		assertFieldTrue(RegistrationPage.abnTextFieldValue);
		assertFieldTrue(RegistrationPage.companyNameTextFieldLabel);
		assertFieldTrue(RegistrationPage.companyNameTextFieldValue);
		
		// This check is made so that the user is scrolled to the 'accountForBuisnessLabel' field. And there we can select/unselect the checkbox
		// (as there is a chance that the checkbox might not be present on the visible area of the screen, so we need to scroll to the area where checkbox is)
		assertFieldTrue(RegistrationPage.accountForBuisnessLabel);
		registrationPage.tapElement(RegistrationPage.accountForBusinessCheckbox);
		assertFieldFalse(RegistrationPage.companyInformationLabel);
		assertFieldFalse(RegistrationPage.abnTextFieldLabel);
		assertFieldFalse(RegistrationPage.abnTextFieldValue);
		assertFieldFalse(RegistrationPage.companyNameTextFieldLabel);
		assertFieldFalse(RegistrationPage.companyNameTextFieldValue);
	}
	
	// checking that Register button is enabled when checkbox is selected
	// Navigating to the disclaimer message area, so that we can select the checkbox
	// Scrolling to the bottom of the screen. This is done twice to reach the very bottom of the screen. 
	// Cause if we give the command once, its scrolling down but its not reaching the very bottom of the screen
	@Test
	public void registerButtonEnabledOnDisclaimerMessage(){
		homePage.taploginIcon();
		loginpage.tapRegisterButton();
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));
		

		registrationPage.swipeScreen("down");		
		registrationPage.swipeScreen("down");		
		AssertJUnit.assertFalse(registrationPage.find(RegistrationPage.registerButton).isEnabled());
		registrationPage.tapElement(RegistrationPage.disclaimerCheckbox);
		AssertJUnit.assertTrue(registrationPage.find(RegistrationPage.registerButton).isEnabled());	
	}
	
	public void enterValue(By locator, String value){
		assertFieldTrue(locator);
		//registrationPage.clearField(locator);
		registrationPage.typeValue(value, locator);
	}
	
	@Test
	public void verifyFieldValidations(){
		homePage.taploginIcon();
		utils.loadTestData(System.getProperty("user.dir") + "/TestData/registrationData.json");
		
		// all empty and then click on Register button, should error out.
		// As in this case all the fields are highlighted in red color and no message is shown, 
		// we check if still user is on the same screen by verifying the 'register' button
		verifyFieldInlineValidations("allEmptyFields", RegistrationPage.registerButton);
		
		// Entering few values and then clicking on Register button.User should still be on 
		// the same screen as the mandatory validations fail.
		verifyFieldInlineValidations("allMandatoryFieldsNotPresent", RegistrationPage.registerButton);
		
		// Entering all values but passwords different and then clicking on Register button.User should still be on 
		// the same screen as the passwords donot match and 'passwords donot match' inline error message is shown on the screen
		verifyFieldInlineValidations("passwordsDonotMatch", RegistrationPage.passwordDoNotMatchErrorMessage);
		
		// Email format not correct
		verifyFieldSubmissionValidations("emailInvalid", RegistrationPage.invalidEmailErrorMessage);
		
		// Invalid Ph num
		verifyFieldSubmissionValidations("PhNoInvalid1", RegistrationPage.invalidPhNumErrorMessage);
		
		// Invalid Ph num
		verifyFieldSubmissionValidations("PhNoInvalid2", RegistrationPage.invalidPhNumErrorMessage);
		
		// Invalid postcode
		verifyFieldSubmissionValidations("postcodeInvalid", RegistrationPage.invalidPostcodeErrorMessage);
		
		// Email entered is already registered
		verifyFieldSubmissionValidations("emailAlreadyTaken", RegistrationPage.emailIdAlreadyExistsErrorMessage);
		
		// password must be minimum 6 characters long
		verifyFieldSubmissionValidations("passwordNotAsPerSpecs", RegistrationPage.invalidPasswordErrorMessage);
		
		// suburb, postcode and country did not match
		verifyFieldSubmissionValidations("suburbPostcodeCountryDonotMatch1", RegistrationPage.invalidSuburbStatePostcodeErrorMessage);
		
		// suburb, postcode and country did not match
		verifyFieldSubmissionValidations("suburbPostcodeCountryDonotMatch2", RegistrationPage.invalidSuburbStatePostcodeErrorMessage);

		// account registration successful
		resgisterAccountSuccessfully("accountRegistrationSuccess", RegistrationPage.thankyouSuccessMessage, RegistrationPage.accountRegisteredSuccessMessage);
	}
	
	public void verifyFieldInlineValidations(String rootElement, By failureMessageShown){
		enterFieldValues(rootElement);
		assertFieldTrue(failureMessageShown);
		registrationPage.tapElement(RegistrationPage.navigateUpButton);
	}
	
	public void verifyFieldSubmissionValidations(String rootElement, By failureMessageShown){
		enterFieldValues(rootElement);
		// Error message should be shown
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(failureMessageShown));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(failureMessageShown));
		registrationPage.tapElement(RegistrationPage.alertOkButton);
		registrationPage.tapElement(RegistrationPage.navigateUpButton);
	}

	public void resgisterAccountSuccessfully(String rootElement, By thankyouMessage, By successMessage){
		enterFieldValues(rootElement);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(thankyouMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(thankyouMessage));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(successMessage));
		registrationPage.tapElement(RegistrationPage.alertOkButton);
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.navigateUpButton));
		registrationPage.tapElement(RegistrationPage.navigateUpButton);
	}

	public void enterFieldValues(String rootElement){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LoginPage.registerButton));
		
		loginpage.tapRegisterButton();
		wait.until(ExpectedConditions.visibilityOfElementLocated(RegistrationPage.passwordTextFieldLabel));
		
		enterValue(RegistrationPage.emailTextFieldValue, utils.readTestData(rootElement, "emailTextFieldValue"));
		enterValue(RegistrationPage.passwordTextFieldValue, utils.readTestData(rootElement, "passwordTextFieldValue"));
		enterValue(RegistrationPage.confirmPasswordTextFieldValue, utils.readTestData(rootElement, "confirmPasswordTextFieldValue"));
		enterValue(RegistrationPage.firstNameTextFieldValue, utils.readTestData(rootElement, "firstNameTextFieldValue"));
		enterValue(RegistrationPage.lastNameTextFieldValue, utils.readTestData(rootElement, "lastNameTextFieldValue"));
		enterValue(RegistrationPage.primaryPhoneNumberTextFieldValue, utils.readTestData(rootElement, "primaryPhoneNumberTextFieldValue"));
		enterValue(RegistrationPage.otherPhoneNumberTextFieldValue, utils.readTestData(rootElement, "otherPhoneNumberTextFieldValue"));
		enterValue(RegistrationPage.buildingTextFieldValue, utils.readTestData(rootElement, "buildingTextFieldValue"));
		enterValue(RegistrationPage.addressTextFieldValue, utils.readTestData(rootElement, "addressTextFieldValue"));
		enterValue(RegistrationPage.suburbTextFieldValue, utils.readTestData(rootElement, "suburbTextFieldValue"));
		enterValue(RegistrationPage.postcodeTextFieldValue, utils.readTestData(rootElement, "postcodeTextFieldValue"));
		//enterValue(RegistrationPage.stateSpinner, utils.readTestData(rootElement, "stateSpinner"));
		registrationPage.swipeScreen("down");		
		registrationPage.swipeScreen("down");
		registrationPage.tapElement(RegistrationPage.disclaimerCheckbox);
		registrationPage.tapElement(RegistrationPage.registerButton);
	}
	
		/* TODO
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBuisnessLabel));
		AssertJUnit.assertTrue(registrationPage.checkElementExists(RegistrationPage.accountForBusinessCheckbox));
	    public static By accountForBusinessCheckbox = By.xpath("//android.widget.CheckBox");
		public static By companyInformationLabel = By.xpath("//android.widget.TextView[@text='Company Information']");
		public static By abnTextFieldLabel = By.xpath("//TextInputLayout[@text='ABN *']");
		public static By abnTextFieldValue = By.xpath("//TextInputLayout[@text='ABN *']//android.widget.EditText");
		public static By companyNameTextFieldLabel = By.xpath("//TextInputLayout[@text='Company Name *']");
		public static By companyNameTextFieldValue = By.xpath("//TextInputLayout[@text='Company Name *']//android.widget.EditText");
		*/
}